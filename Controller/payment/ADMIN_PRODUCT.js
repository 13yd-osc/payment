[//
    async function( kuo,Context,context){
        let LANG = this.LANG();
        let $DATA = Object.assign({},this.$DATA);
        let $hash = 'safetoken/'+kuo.adminid;
        $DATA.token = Mode("Tools").Uuid();
        await  Mem.Set($hash,$DATA.token);
        let  $kongzhi = kuo.Path.length > 0?kuo.Path[kuo.Path.length-1]:"get";
        let  $db = await db('payment_product');
        let  $_GET = kuo.get;
        let  $_POST = kuo.post;
        let  $chushi = true; 
        let  $features = await Kuoplus(kuo.class);
        if($kongzhi == 'get'){
    
            let $page = Mode("Tools").Int($_GET['page']?$_GET['page']:1);
            let $limitx  =  Mode("Tools").Int($_GET['limit']?$_GET['limit']:10);
            if($limitx < 10 ){
                $limitx = 10;
            }else if($limitx > 100 ){
                $limitx = 100;
            }
            let $where ={};
            if(isset($_GET['off']) && $_GET['off'] != ""){
                $chushi = false; 
                $where['off'] = $_GET['off'];
            }
            if(isset($_GET['name']) && $_GET['name'] != ""){
                $chushi = false; 
                $where['name LIKE'] ="%"+$_GET['name']+"%";
            }

            let $data  = await $db.Where($where).Limit($limitx,$page).Order("id desc").Select();
            let $total = await $db.Where($where).Total();
            if(!$data){
                $data= [];
            }
            $DATA.code = 0;
            $DATA.count = $total;
            if($page == 1 && $chushi){
              
            }
    
            $DATA.data = $data;
    
    
        }else if($kongzhi == 'put'){
            let $id = Mode("Tools").Int($_POST['id']?$_POST['id']:0);
            let $data = await $db.Where({id: $id}).Find();
            if(!$data){
                $DATA.code = -1;
                $DATA.msg = LANG.admin_id_no;
                Context.body = JSON.stringify($DATA);
                return ;
            }
            $_POST['atime']= Mode("Tools").Time();

            let $fan = await $db.Where({id: $id}).Update($_POST);
            if($fan){
                await Kuolog('adminlog',kuo.adminid,3,{'yuan':$data,'data':$_POST},kuo.func_,kuo.class,kuo.ip);
                $DATA.data = $_POST;
                $DATA.code =1;
                $DATA.msg = LANG.put_ok;
            }else{
                $DATA.code =-1;
                $DATA.msg = LANG.put_no;
            }
    
            
        }else if($kongzhi == 'add'){
    
            $_POST['atime']= Mode("Tools").Time();
            let $fan =await $db.Insert($_POST);
            if($fan){
                $DATA.code = 1;
                $DATA.msg = LANG.add_ok;
                await Kuolog('adminlog',kuo.adminid,4, $_POST ,kuo.func_,kuo.class,kuo.ip);
            }else{
                $DATA.code = -1;
                $DATA.msg = LANG.add_no;
            }
    
    
        }else if($kongzhi == 'del'){
    
            let $id = Mode("Tools").Int($_POST['id']?$_POST['id']:0);
            let $data = await $db.Where({id: $id}).Find();
    
            if(!$data){
                $DATA.code = -1;
                $DATA.msg = LANG.admin_id_no;
                Context.body = JSON.stringify($DATA);
                return ;
            }
    
            let $fan = await $db.Where({id: $id}).Delete();
            if($fan){
               
                await  Kuolog('adminlog',kuo.adminid,5, $data ,kuo.func_,kuo.class,kuo.ip);
                $DATA.code = 1;
                $DATA.msg = LANG.del_ok;
            }else{
                $DATA.code = -1;
                $DATA.msg = LANG.del_no;
            }
    
    
        }
        
        Context.body = JSON.stringify($DATA);
    }
    
]