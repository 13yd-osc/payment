[//
    async function( kuo,Context,context){
        let LANG = this.LANG();
        let $DATA = Object.assign({},this.$DATA);
        let $hash = 'safetoken/'+kuo.adminid;
        $DATA.token = Mode("Tools").Uuid();
        await  Mem.Set($hash,$DATA.token);
        let  $kongzhi = kuo.Path.length > 0?kuo.Path[kuo.Path.length-1]:"get";
        let  $db = await db('payment_code');
        let  $_GET = kuo.get;
        let  $_POST = kuo.post;
        let  $chushi = true; 
        let  $features = await Kuoplus(kuo.class);
        if($kongzhi == 'get'){
    
            let $page = Mode("Tools").Int($_GET['page']?$_GET['page']:1);
            let $limitx  =  Mode("Tools").Int($_GET['limit']?$_GET['limit']:10);
            if($limitx < 10 ){
                $limitx = 10;
            }else if($limitx > 100 ){
                $limitx = 100;
            }
            let $where ={};
            if(isset($_GET['type']) && $_GET['type'] != ""){
                $chushi = false; 
                $where['type'] = $_GET['type'];
            }
        
            if(isset($_GET['id']) && $_GET['id'] != ""){
                $chushi = false; 
                $where['id'] = $_GET['id'];
            }
        
            if(isset($_GET['moeny']) && $_GET['moeny'] != ""){
                $chushi = false; 
                $where['moeny'] = $_GET['moeny'];
            }
            if(isset($_GET['orderid']) && $_GET['orderid'] != ""){
                $chushi = false; 
                $where['orderid'] = $_GET['orderid'];
            }
            if(isset($_GET['off']) && $_GET['off'] != ""){
                $chushi = false; 
                if($_GET['off'] > 0){
                    $where['orderid >'] = 0;
                }else{
                    $where['orderid'] = 0;
                }
            }
            let $data  = await $db.Where($where).Limit($limitx,$page).Order("id desc").Select();
            let $total = await $db.Where($where).Total();
            if(!$data){
                $data= [];
            }
            $DATA.code = 0;
            $DATA.count = $total;
            if($page == 1 && $chushi){
                $DATA['type'] = $features['configure']['支付方式']?$features['configure']['支付方式']:[];
            }

            $DATA.data = $data;
    
        }else if($kongzhi == 'put'){
            let $id = Mode("Tools").Int($_POST['id']?$_POST['id']:0);
            let $data = await $db.Where({id: $id}).Find();
            if(!$data){
                $DATA.code = -1;
                $DATA.msg = LANG.admin_id_no;
                Context.body = JSON.stringify($DATA);
                return ;
            }
            if($data['orderid'] > 0){
                $DATA.code = -1;
                $DATA.msg = LANG.code_yong;
                Context.body = JSON.stringify($DATA);
                return ;
            }
            if($_POST['id']){
                delete($_POST['id']);
            }
            if($_POST['atime']){
                delete($_POST['atime']);
            }
            let $fan = await $db.Where({id: $id}).Update($_POST);
            if($fan){
                await Kuolog('adminlog',kuo.adminid,3,{'yuan':$data,'data':$_POST},kuo.func_,kuo.class,kuo.ip);
                $DATA.data = $_POST;
                $DATA.code =1;
                $DATA.msg = LANG.put_ok;
            }else{
                $DATA.code =-1;
                $DATA.msg = LANG.put_no;
            }
    
            
        }else if($kongzhi == 'add'){
            if(isset($_POST['pldel']) && $_POST['pldel']!= ""){


                let $type = Mode("Tools").Int($_POST['type']?$_POST['type']:0);
                let $codeimg = $_POST['codeimg']?$_POST['codeimg']:"";
                let $remarks = $_POST['remarks']?$_POST['remarks']:"";
                let $num = Mode("Tools").Int($_POST['num']?$_POST['num']:10);
                let $moeny = $_POST['moeny']?$_POST['moeny']:"";
                if($moeny == ""){
                    $DATA.code = -1;
                    $DATA.msg = LANG.jine_fanerror;
                    Context.body = JSON.stringify($DATA);
                    return ;
                }
                //每个金额的数量
                if($num < 1 || $num >= 100){
                    $DATA.code = -1; $DATA.msg = LANG.jine_ge;Context.body = JSON.stringify($DATA);
                    return ;
                }
                //每次增加
                let $paymoeny = Mode("Tools").Jine($_POST['paymoeny']?$_POST['paymoeny']:0.01);

                if (strpos($moeny , '-') !== false ) {
                    let xx = explode("-",$moeny );
                    let $d = Mode("Tools").Jine(xx[0]);
                    let $v= Mode("Tools").Jine(xx[1]);
                    let $x = $d;
                    let $y = $v;
                    if($y < $x){
                        $x = $v;
                        $y = $d; 
                    }
                    let $jiange = Mode("Tools").Int($_POST['jiange']?$_POST['jiange']:1);
                    if($jiange < 1){
                        $DATA.code = -1; $DATA.msg = LANG.jine_jiange;Context.body = JSON.stringify($DATA);
                        return ;
                     
                    }
        
                    if($y/$jiange > 500){
                        $DATA.code = -1; $DATA.msg = LANG.make_duo;Context.body = JSON.stringify($DATA);
                        return ;
                    }
                    for(let $i = $x;$i<=$y; $i++ ){
                        if($i%$jiange == 0 || $i == $x ){
                            for(let $j=0;$j< $num;$j++){
        
                                let $shuju ={
                                    'type':$type,
                                    'codeimg':$codeimg,
                                    'remarks':$remarks,
                                    'moeny':$i,
                                    'paymoeny':$i+$j*$paymoeny
                                };
                                await $db.Insert($shuju);
                            }
                        }
                    }
        
                }else{
        
                    let $jine = explode(",",$moeny);
                    for( let mm__ in $jine   ){
                        let $jine_ =$jine[mm__];
                        $jine_ = Mode("Tools").Jine($jine_);
                        for(let $j=0;$j< $num;$j++){
                             let $shuju ={
                                'type':$type,
                                'codeimg':$codeimg,
                                'remarks':$remarks,
                                'moeny':$jine_,
                                'paymoeny':$jine_+$j*$paymoeny
                            };
                            await $db.Insert($shuju);
                        }
                    }
        
                }

                $DATA.code = 1;
                $DATA.msg = LANG.pl_make;
                await Kuolog('adminlog',kuo.adminid,4, $_POST ,kuo.func_,kuo.class,kuo.ip);
                Context.body = JSON.stringify($DATA);
                return ;
            }
            $_POST['atime'] = 0;
            let $fan =await $db.Insert($_POST);
            if($fan){
                $DATA.code = 1;
                $DATA.msg = LANG.add_ok;
                await Kuolog('adminlog',kuo.adminid,4, $_POST ,kuo.func_,kuo.class,kuo.ip);
            }else{
                $DATA.code = -1;
                $DATA.msg = LANG.add_no;
            }
    
    
        }else if($kongzhi == 'del'){
    
            let $id = Mode("Tools").Int($_POST['id']?$_POST['id']:0);
            let $data = await $db.Where({id: $id}).Find();
    
            if(!$data){
                $DATA.code = -1;
                $DATA.msg = LANG.admin_id_no;
                Context.body = JSON.stringify($DATA);
                return ;
            }

            if($data['orderid'] > 0){
                $DATA.code = -1;
                $DATA.msg = LANG.code_yong;
                Context.body = JSON.stringify($DATA);
                return ;
            }
    
            let $fan = await $db.Where({id: $id}).Delete();
            if($fan){
                await  Kuolog('adminlog',kuo.adminid,5, $data ,kuo.func_,kuo.class,kuo.ip);
                $DATA.code = 1;
                $DATA.msg = LANG.del_ok;
            }else{
                $DATA.code = -1;
                $DATA.msg = LANG.del_no;
            }
        }
        
        Context.body = JSON.stringify($DATA);
    }
    
]