[//
    async function( kuo,Context,context){
        let LANG = this.LANG();
        let $DATA_ = Object.assign({},this.$DATA);
        let $hash = 'safetoken/'+kuo.adminid;
        $DATA_.token = Mode("Tools").Uuid();
        await  Mem.Set($hash,$DATA_.token);
        let  $kongzhi = kuo.Path.length > 0?kuo.Path[kuo.Path.length-1]:"get";
        let  $db = await db('payment_notice');
        let  $_GET = kuo.get;
        let  $_POST = kuo.post;
        let  $chushi = true; 
        let  $features = await Kuoplus(kuo.class);
        if($kongzhi == 'get'){
            
            let $TJNUM =  Mode("Tools").Int($features['configure']['统计天数']['0']?$features['configure']['统计天数']['0']:7);
            if($TJNUM < 1){
                $TJNUM = 2;
            }
            if($TJNUM > 31){
                $TJNUM = 31;
            }
            $DATA_.code = 1;
           
            let $DATA = {
                "总统计" : {
                    'APP通知':{},
                    '匹配码库':{},
                    '充值订单':{},
                    '产品仓库':{}
                },
                "产品销量" : {
        
                },
                "订单统计" : {
        
                },
                "APP通知" : {
        
                }
            };
            $db = await $db.SetTable('payment_notice');
            $DATA['总统计']['APP通知']['总数'] = ""+await $db.Total();
            let $fan = await $db.Qurey("SELECT sum(moeny) as num from "+$db.Biao());
            let $daozhang = Mode("Tools").Jine($fan['0']['num']?$fan['0']['num']:0);
            $DATA['总统计']['APP通知']['到账'] = "<span style='color:red;'>"+$daozhang+"</span>";
            $fan = await $db.Qurey("SELECT sum(moeny) as num from "+$db.Biao()+" "+$db.Wherezuhe({'orderid >':0}));
            $DATA['总统计']['APP通知']['完成'] = Mode("Tools").Jine($fan['0']['num']?$fan['0']['num']:0);
            $DATA['总统计']['APP通知']['待用'] = ($daozhang- $DATA['总统计']['APP通知']['完成']);
            $db = await $db.SetTable('payment_code');
            $DATA['总统计']['匹配码库']['总数'] = ""+ await $db.Total();
            $DATA['总统计']['匹配码库']['待用'] = await $db.Where({'orderid':0}).Total();
            $DATA['总统计']['匹配码库']['已用'] = ""+ ($DATA['总统计']['匹配码库']['总数']- $DATA['总统计']['匹配码库']['待用']);
            $db = await $db.SetTable('payment_order');
            $DATA['总统计']['充值订单']['总数']  = ""+ await $db.Total();
            $DATA['总统计']['充值订单']['处理']  = ""+ await $db.Where({'off IN':'0,1'}).Total();
            $DATA['总统计']['充值订单']['成功'] = ""+ await $db.Where({'off':'2'}).Total();
            $fan = await $db.Qurey("SELECT sum(moeny) as moeny,sum(paymoeny) as paymoeny from "+$db.Biao()+" "+$db.Wherezuhe({'off':'2'}));
            $DATA['总统计']['充值订单']['金额'] = Mode("Tools").Jine($fan['0']['moeny']?$fan['0']['moeny']:0)+' ^ <span style="color:Red;">'+Mode("Tools").Jine($fan['0']['paymoeny']?$fan['0']['paymoeny']:0)+'</span>';
            $db= await $db.SetTable('payment_product');
            $DATA['总统计']['产品仓库']['总数']  = ""+ await $db.Total();
            $DATA['总统计']['产品仓库']['正常']  = await $db.Where({'off':2}).Total();
            $db= await $db.SetTable('payment_order');
            $DATA['总统计']['产品仓库']['销量']  = ""+await $db.Where({'productid >':0,'off':2}).Total();
            $fan =  await $db.Qurey("SELECT sum(moeny) as moeny,sum(paymoeny) as paymoeny from "+$db.Biao()+" "+$db.Wherezuhe({'productid >':0,'off':2}));
            $DATA['总统计']['产品仓库']['销量金额']  = Mode("Tools").Jine($fan['0']['moeny']?$fan['0']['moeny']:0)+' ^ <span style="color:Red;">'+Mode("Tools").Jine($fan['0']['paymoeny']?$fan['0']['paymoeny']:0)+'</span>';
            

            $db= await $db.SetTable('payment_order');
          
            for(let $i = 0;$i< $TJNUM;$i++){
                let $zuidi =   strtotime(Mode("Tools").Date("Y-m-d", Mode("Tools").Time()-3600*24*$i  ));
                let $dqngqi =  strtotime(Mode("Tools").Date("Y-m-d", $zuidi+3600*24));
                let $today = Mode("Tools").Date("Y-m-d",$zuidi);
                $DATA['订单统计'][$today] = {};
                $DATA['订单统计'][$today]['总数']  = ""+await $db.Where({'atime >':$zuidi,'atime <=':$dqngqi}).Total();
                $DATA['订单统计'][$today]['处理中']= ""+await $db.Where({'off IN':'0,1','atime >':$zuidi,'atime <=':$dqngqi}).Total();
                $DATA['订单统计'][$today]['成功'] = ""+await $db.Where({'off':'2','atime >':$zuidi,'atime <=':$dqngqi}).Total();
                $fan =  await $db.Qurey(" SELECT sum(moeny) as moeny,sum(paymoeny) as paymoeny from "+$db.Biao()+" "+$db.Wherezuhe({'off':'2','atime >':$zuidi,'atime <=':$dqngqi}));
                $DATA['订单统计'][$today]['订单金额'] =""+Mode("Tools").Jine($fan['0']['moeny']?$fan['0']['moeny']:0);
                $DATA['订单统计'][$today]['支付金额'] =""+Mode("Tools").Jine($fan['0']['paymoeny']?$fan['0']['paymoeny']:0);
            }

            $db = await $db.SetTable('payment_notice');
            for(let $i = 0;$i< $TJNUM;$i++){
                let $zuidi =   strtotime(Mode("Tools").Date("Y-m-d", Mode("Tools").Time()-3600*24*$i  ));
                let $dqngqi =  strtotime(Mode("Tools").Date("Y-m-d", $zuidi+3600*24));
                let $today = Mode("Tools").Date("Y-m-d",$zuidi);
                $DATA['APP通知'][$today] = {};
                $DATA['APP通知'][$today]['总数'] = ""+await $db.Where({'atime >':$zuidi,'atime <=':$dqngqi}).Total();
                $fan = await $db.Qurey(" SELECT sum(moeny) as num from "+$db.Biao()+" "+$db.Wherezuhe({ 'atime >':$zuidi,'atime <=':$dqngqi }));
                $DATA['APP通知'][$today]['到账金额'] = ""+Mode("Tools").Jine($fan['0']['num']?$fan['0']['num']:0);
                $fan = await $db.Qurey(" SELECT sum(moeny) as num from "+$db.Biao()+" "+$db.Wherezuhe({ 'orderid >': 0,'atime >':$zuidi,'atime <=':$dqngqi }));
                $DATA['APP通知'][$today]['处理金额'] = ""+Mode("Tools").Jine($fan['0']['num']?$fan['0']['num']:0);
                $DATA['APP通知'][$today]['未处理金额'] =""+($DATA['APP通知'][$today]['到账金额']- $DATA['APP通知'][$today]['处理金额']);
            
            }
            $db = await $db.SetTable('payment_product');
            let $chanpin = await $db.Zhicha("id,name,off").Limit(18).Order("id desc").Select();
            $db = await $db.SetTable('payment_order');
            if($chanpin){
                for(var xx_ in $chanpin ){
                    let $kuo_ = $chanpin[xx_];
                    let $id = $kuo_['id'];
                    $DATA['产品销量'][$kuo_['name']] = {};
                    $DATA['产品销量'][$kuo_['name']]['销量']  = await $db.Where({'productid':$id ,'off':2}).Total();
                    $DATA['产品销量'][$kuo_['name']]['状态']  = $kuo_['off'] =='2'?'<span style="color:green;">正常</span>':'<span style="color:red;">关闭</span>';
        
                    $fan = await  $db.Qurey(" SELECT sum(moeny) as moeny,sum(paymoeny) as paymoeny from "+$db.Biao()+" "+$db.Wherezuhe({'productid':$id,'off':2}));
                    $DATA['产品销量'][$kuo_['name']]['销量金额']  =""+Mode("Tools").Jine($fan['0']['moeny']?$fan['0']['moeny']:0);
                    $DATA['产品销量'][$kuo_['name']]['可得金额']  = ""+Mode("Tools").Jine($fan['0']['paymoeny']?$fan['0']['paymoeny']:0);
                }
            }

            $DATA_.data = $DATA;
    
        }else if($kongzhi == 'put'){
        }else if($kongzhi == 'add'){
        }else if($kongzhi == 'del'){
        }
        
        Context.body = JSON.stringify($DATA_);
    }
    
]