[//
    async function( kuo,Context,context){
        Context.headers["content-type"] = 'text/html; charset=UTF-8';
        let LANG = this.LANG();
        let $DATA = Object.assign({},this.$DATA);
        let $CANSHU = kuo.Path;
        let $THIS = this;
        let $features  = await Kuoplus(kuo.class);   
        let $MCHID = $features['configure']['APP通知']['0']?$features['configure']['APP通知']['0']:"";
        let $MCKEY = $features['configure']['APP通知']['1']?$features['configure']['APP通知']['1']:"";
        let $TYPE = $features['configure']['支付方式']?$features['configure']['支付方式']:[];

        if($MCHID == "" || strlen($MCKEY) < 12){
            $DATA.code = -1; $DATA.msg = '异步通信关闭'; Context.body = JSON.stringify($DATA);
            return ;
        }
        let $MONEY = Mode("Tools").Int($CANSHU['0']);
        let $SIGN  = $CANSHU['4']?$CANSHU['4']:"";
        let $TOKEN = $CANSHU['3']?$CANSHU['3']:"";
        let $LEIXIN = Mode("Tools").Int($CANSHU['2']);
        let $TIME  = $CANSHU['1']?$CANSHU['1']:"";
        let TIME = Mode("Tools").Time();
        if($MONEY <= 0){
            $DATA.code = -1; $DATA.msg = '金额错误'; Context.body = JSON.stringify($DATA);
            return ;
            
        }
        if(!isset( $TYPE[$LEIXIN] )){
            $DATA.code = -1; $DATA.msg = '分类错误'; Context.body = JSON.stringify($DATA);
            return ;
            
        }
        if(TIME - $TIME > 15){
            $DATA.code = -1; $DATA.msg = '超时'; Context.body = JSON.stringify($DATA);
            return ;
        
        }
        if($SIGN != Md5($MCHID+"#"+$MONEY+"#"+$LEIXIN+"#"+$TIME+"#"+$TOKEN+"#"+$MCKEY) ){
            $DATA.code = -1; $DATA.msg = '签名错误'; Context.body = JSON.stringify($DATA);
            return ;
        }
        $MONEY =  Mode("Tools").Jine($MONEY/100);
        let $security = Md5($LEIXIN+"#"+$MONEY+"#"+$SIGN);
        let $db = await db("payment_notice");
        let $noticeid = await $db.Insert({
            'type':$LEIXIN,
            'moeny':$MONEY,
            'atime':TIME,
            'security':$security 
        });
        if(!$noticeid){
            $DATA.code = -1; $DATA.msg = '录入失败'; Context.body = JSON.stringify($DATA);
            return ;
        }
        $db = await $db.SetTable("payment_code");
        let $code = await $db.Where({'type':$LEIXIN,'paymoeny':$MONEY}).Find();
        if(!$code){
            $DATA.code = -1; $DATA.msg = '二维码错误'; Context.body = JSON.stringify($DATA);
            return ;
        }

        if( $code['orderid'] == 0 ){
            $DATA.code = -1; $DATA.msg = '订单错误'; Context.body = JSON.stringify($DATA);
            return ;
        }
        
        $db = await $db.SetShiwu(true).SetTable("payment_notice");

        let $sql = await $db.Where({'id': $noticeid }).Update({'codeid':$code['id'],'orderid' :$code['orderid'] });
        $db = await $db.SetTable("payment_code");
        $sql += await $db.Where({'id': $code['id'] }).Update({'orderid':0,'atime' : 0 });
        $db = await $db.SetTable("payment_order");
        $sql += await $db.Where({'id': $code['orderid'] ,'off' : 1 }).Update({'off':2,'ctime' : TIME,'noticeid':$noticeid,'paymoeny':$MONEY });
        let $fan = await $db.ShiWu($sql);
        if(!$fan){
            $DATA.code = -1; $DATA.msg = '更改错误'; Context.body = JSON.stringify($DATA);
            return ;
        }
        await $THIS.Notice_Post($code['orderid'],$features);
        $DATA.code = 1;
        Context.body = JSON.stringify($DATA);
    }
    
]