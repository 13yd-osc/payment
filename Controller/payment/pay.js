[//
    async function( kuo,Context,context){
        let LANG = this.LANG();
        let $DATA_ = Object.assign({},this.$DATA);
        let  $_GET = kuo.get;
        let  $_POST = kuo.post;
        let  $features = await Kuoplus(kuo.class);
        let $DATA = {
            'orderid': Mode("Tools").Xss($_POST['orderid']?$_POST['orderid']:""),
            'type':  Mode("Tools").Int($_POST['type']),
            'subject': Mode("Tools").Xss($_POST['subject']?$_POST['subject']:""),
            'productid': Mode("Tools").Int($_POST['productid']),
            'uid': Mode("Tools").Int($_POST['uid']),
            'notify': Mode("Tools").Xss($_POST['notify']?$_POST['notify']:""),
            'return': Mode("Tools").Xss($_POST['return']?$_POST['return']:""),
            'moeny': Mode("Tools").Int($_POST['moeny']?$_POST['moeny']:1),
            'time': Mode("Tools").Int($_POST['time']?$_POST['time']:Mode("Tools").Time()),
            'remarks': Mode("Tools").Xss($_POST['remarks']?$_POST['remarks']:""),
            'mchid': $_POST['mchid']? $_POST['mchid']:"",
            'sign': $_POST['sign']?$_POST['sign']:""
        };

        let $MCHID = $features['configure']['订单录入']['0']?$features['configure']['订单录入']['0']:"";
        let $MCKEY = $features['configure']['订单录入']['1']?$features['configure']['订单录入']['1']:"";
        //支付方式读取
        let $TYPE = $features['configure']['支付方式']?$features['configure']['支付方式']:[];
        //过期时间
        let $GTIME =  Mode("Tools").Int($features['configure']['过期时间']['0']?$features['configure']['过期时间']['0']:300);
        if(!isset($TYPE[$DATA['type']])){
            $DATA_.code = -1;$DATA_.msg = LANG.type_error;Context.body = JSON.stringify($DATA_);
            return ;
        }
        if(!isset($features['configure']['订单录入']) || $MCHID == "" || $MCKEY == ""){

            $DATA_.code = -1; $DATA_.msg = LANG.close_off; Context.body = JSON.stringify($DATA_);
            return ;
            
        }
        if($DATA['mchid'] != $MCHID){
            $DATA_.code = -1; $DATA_.msg = LANG.mchid_error; Context.body = JSON.stringify($DATA_);
            return ;
        }
        if( strlen($DATA['orderid']) < 12){
            $DATA_.code = -1; $DATA_.msg = LANG.orderid_error; Context.body = JSON.stringify($DATA_);
            return ;
        }
        if($DATA['moeny'] < 0.01){
            $DATA_.code = -1; $DATA_.msg = LANG.moeny_error; Context.body = JSON.stringify($DATA_);
            return ;
         
        }
        if($DATA['time'] < Mode("Tools").Time()){
            if( Mode("Tools").Time() - $DATA['time'] >2){
                $DATA_.code = -1; $DATA_.msg = LANG.time_error; Context.body = JSON.stringify($DATA_);
                return ;
               
            }
        }else  if($DATA['time'] > Mode("Tools").Time() ){
            $DATA_.code = -1; $DATA_.msg = LANG.time_error; Context.body = JSON.stringify($DATA_);
            return ;
        }
        let $sign = Md5($DATA['uid']+$DATA['mchid']+$DATA['type']+$DATA['orderid']+$DATA['moeny'] +$DATA['time'] +$DATA['remarks']+$DATA['subject']+$DATA['productid']+$MCKEY);
        if($sign != $DATA['sign']){
            $DATA_.code = -1; $DATA_.msg = LANG.sign_error; Context.body = JSON.stringify($DATA_);
            return ;
        }
        let $back = $_POST['back']?$_POST['back']:"html";
        let $UHASH = 'token/'+$DATA['uid'];
        if($DATA['uid'] > 0 ){
            //读取缓存数据
            let $Memdata = await Mem.Get($UHASH);
            if($Memdata){
                if($back == "json"){
                    $DATA_.data = $Memdata;
                    $DATA_.code = 1; $DATA_.msg = "ok"; Context.body = JSON.stringify($DATA_);
                    return ;
                }else{

                    Context.statusCode = 302;
                    Context.headers["Location"] = $Memdata;
                    Context.body = "";
                    return ;
                }
            }
        };

        let $productid = {};
        let $db = await db("payment_order");
        if($DATA['productid'] > 0){
            $db = await $db.SetTable("payment_product");
            $productid = await $db.Where({'id' :$DATA['productid']} ).Find();
            if(!$productid){
                $DATA_.code = -1; $DATA_.msg = LANG.productid_error; Context.body = JSON.stringify($DATA_);
                return ;
            }
            if($productid['off'] != 2){
                $DATA_.code = -1; $DATA_.msg = LANG.productid_off; Context.body = JSON.stringify($DATA_);
                return ;
                
            }
            if($productid['moeny'] <= 0){
                $DATA_.code = -1; $DATA_.msg = LANG.moeny_error; Context.body = JSON.stringify($DATA_);
                return ;
            }
            
            $DATA['moeny'] = $productid['moeny'];

        }else{

            if(strpos($DATA['notify'] , '//') === false ){
                
                $DATA_.code = -1; $DATA_.msg = LANG.notify_no; Context.body = JSON.stringify($DATA_);
                return ;
            }
        }

        $DATA['off']=0;
        $DATA['atime'] = Mode("Tools").Time();
        $DATA['ip'] = kuo.ip;
        $DATA['anget'] = platform(Mode("Tools").Xss(kuo.agent));
        $db = await  $db.SetTable("payment_order");
        let $orderid = await $db.Insert($DATA);
        if(!$orderid){
            DATA_.code = -1; $DATA_.msg = LANG.add_no; Context.body = JSON.stringify($DATA_);
            return ;
        }
        $db = await  $db.SetTable("payment_code");
        //金额 分类读取 二维码
        let $code = await $db .Where({'type':$DATA['type'], 'moeny':$DATA['moeny'],'atime': 0 }).Find();
        //生成跳转查询url
        let $TIAOZHUAN = {
            'orderid':$orderid,
            'token':Md5(orderid()),
            'uuid':Mode("Tools").Uuid(),
            'time':$DATA['atime']+$GTIME
        };
        $TIAOZHUAN['sign'] = Md5($TIAOZHUAN['time']+$MCHID+$TIAOZHUAN['orderid']+$TIAOZHUAN['token']+$MCKEY)+Md5($TIAOZHUAN['time']+$TIAOZHUAN['uuid']+$MCKEY);
        //跳转URL 组合
        let $tiaourl = KuoLink([ kuo.class,'payfind',$TIAOZHUAN['uuid'],$TIAOZHUAN['orderid'],$TIAOZHUAN['token'],$TIAOZHUAN['sign'],$TIAOZHUAN['time']]);

        if(!$code){
            //异步获取结果 删除创建好的订单
            if($back == 'json'){
                $db = await $db.SetTable("payment_order");
                await $db.Where({'id':$orderid}).Delete();
                DATA_.code = -1; $DATA_.msg = LANG.code_no; Context.body = JSON.stringify($DATA_);
                return ;
                
            }
            //写入缓存
            if($DATA['uid'] > 0 ){
                await Mem.Set($UHASH,$tiaourl,$GTIME);
            }

            Context.statusCode = 302;
            Context.headers["Location"] = $tiaourl;
            Context.body = "";
            return ;
        }
        $db.SetShiwu(true);
        $db = await $db.SetTable("payment_code");
        let $sql = await  $db.Where({'atime': 0,'id': $code['id']}).Update({'atime':$DATA['atime'],'orderid': $orderid});
        $db = await $db.SetTable("payment_order");
        $sql += await $db.Where({'id':$orderid}).Update({'off': 1,'codeid': $code['id'],'atime': $DATA['atime']});
        let $fan = await $db.ShiWu($sql);
        if($back == 'json'){
            if($fan){
                let $codeimg = $code['codeimg'] != '' ? $code['codeimg']: ($features['configure'][$TYPE[$DATA['type']]]['0']?$features['configure'][$TYPE[$DATA['type']]]['0']:"");

                if($codeimg == ""){
                    $db = await $db.SetTable("payment_order");
                    await $db.Where({'id':$orderid }).Delete();
                    $db = await $db.SetTable("payment_code");
                    await $db.Where({'id':$code['id']}).Update({'atime':0,'orderid':0});
                    DATA_.code = -1; $DATA_.msg = LANG.code_error; Context.body = JSON.stringify($DATA_);
                    return ;
                }

                let $canshu = {
                    'moeny':$DATA['moeny'], //订单金额
                    'paymoeny' :$code['paymoeny'] , //匹配金额
                    'payurl': $codeimg,    //二维码图片
                    'id':$orderid,    //系统订单
                    'miao':$GTIME,    //过期秒数
                    'time': $DATA['atime']+$GTIME //过期时间戳
                };
                if($DATA['uid'] > 0 ){
                    await Mem.Set($UHASH,$canshu,$GTIME);
                }
                DATA_.code = 1; DATA_.data = $canshu;$DATA_.msg = "ok"; Context.body = JSON.stringify($DATA_);
                return ;
            }else{
                $db = await $db.SetTable("payment_order");
                await $db.Where({'id': $orderid }).Delete();
                DATA_.code = -1; $DATA_.msg = LANG.fenpei_error; Context.body = JSON.stringify($DATA_);
                return ;
            }
        }

        if($DATA['uid'] > 0 ){
            await Mem.Set($UHASH,$tiaourl,$GTIME);
        }

        Context.statusCode = 302;
        Context.headers["Location"] = $tiaourl;
        Context.body = "";
        return ;
    }
    
]