[//
    async function( kuo,Context,context){
        Context.headers["content-type"] = 'text/html; charset=UTF-8';
        let LANG = this.LANG();
        let $DATA = Object.assign({},this.$DATA);
        let $CANSHU = kuo.Path;
        let $THIS = this;
        let $features  = await Kuoplus(kuo.class);   
        let $MCHID = $features['configure']['订单录入']['0']?$features['configure']['订单录入']['0']:"";
        let $MCKEY = $features['configure']['订单录入']['1']?$features['configure']['订单录入']['1']:"";
        let $TYPE = $features['configure']['支付方式']?$features['configure']['支付方式']:[];
        let $ORDERID = $CANSHU['0']?$CANSHU['0']:"";
        let $QMING =$CANSHU['1']?$CANSHU['1']:"";
        let $SIGN = $CANSHU['2']?$CANSHU['2']:"";
        if($ORDERID == "" || strlen($ORDERID) < 12){
            $DATA.code = -1; $DATA.msg = '订单错误'; Context.body = JSON.stringify($DATA);
            return ;
        }
        if(strlen($SIGN) != 64){
            $DATA.code = -1; $DATA.msg = '签名错误'; Context.body = JSON.stringify($DATA);
            return ;
            
        }
  
        if( $QMING !=  Md5($MCHID+$SIGN+$ORDERID+$MCKEY) ){
            $DATA.code = -1; $DATA.msg = '签名错误'; Context.body = JSON.stringify($DATA);
            return ;
        }

        let $db = await db("payment_order");
        let $DATA_ = await $db.Zhicha("id,type,off,moeny,paymoeny").Where({'orderid':$ORDERID}).Find();
        if(!$DATA_){
            $DATA.code = -1; $DATA.msg = '订单不存在'; Context.body = JSON.stringify($DATA);
            return ;
        }
        
        
        if($DATA_['off'] == '0'){
            let $DATAatime = Mode("Tools").Time();
            $db = await $db.SetTable("payment_code");
            //金额 分类读取 二维码
            let $code = await $db .Where({'type':$DATA_['type'], 'moeny':$DATA_['moeny'],'atime': 0 }).Find();
            if($code){
                let $orderid = $DATA_['id'];
                let $sql = await  $db.SetShiwu(true).Where({'atime': 0,'id': $code['id']}).Update({'atime':$DATAatime,'orderid': $orderid});
                $db = await $db.SetTable("payment_order");
                $sql += await $db.Where({'id':$orderid}).Update({'off': 1,'codeid': $code['id'],'atime': $DATAatime});
                let $fan = await $db.ShiWu($sql);
                if($fan){
                    let $GTIME =  Mode("Tools").Int($features['configure']['过期时间']['0']?$features['configure']['过期时间']['0']:300);
                    let $TIAOZHUAN = {
                        'orderid':$orderid,
                        'token':Md5(orderid()),
                        'uuid':Mode("Tools").Uuid(),
                        'time':$DATAatime+$GTIME
                    };
                    $TIAOZHUAN['sign'] = Md5($TIAOZHUAN['time']+$MCHID+$TIAOZHUAN['orderid']+$TIAOZHUAN['token']+$MCKEY)+Md5($TIAOZHUAN['time']+$TIAOZHUAN['uuid']+$MCKEY);
                    $DATA_['tiaourl'] = KuoLink([ kuo.class,'payfind',$TIAOZHUAN['uuid'],$TIAOZHUAN['orderid'],$TIAOZHUAN['token'],$TIAOZHUAN['sign'],$TIAOZHUAN['time']]);
                    let codeimg = $code['codeimg'] != '' ? $code['codeimg']: ($features['configure'][$TYPE[$DATA_['type']]]['0']?$features['configure'][$TYPE[$DATA_['type']]]['0']:"");
                    $DATA_['paymoeny'] =$code['paymoeny'];
                    $DATA_['payurl'] = pichttp(codeimg);
                    $DATA_['id'] = $orderid;
                    $DATA_['miao'] = $GTIME;
                    $DATA_['off'] = 1;
                    $DATA_['time'] = $DATAatime +$GTIME ;
                }
            }
        }
        

        $DATA.code = 1;
        $DATA.data = $DATA_;
        Context.body = JSON.stringify($DATA);
    }
    
]