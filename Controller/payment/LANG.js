[//语言包

function(lang){
    if(!lang){
        lang  = Language;
    }
    let YUYAN = {
        "cn":{
           
            "put_ok":"修改成功",
            "put_no":"修改失败",
            "add_ok":"新增成功",
            "add_no":"新增失败",
            "del_ok":"删除成功",
            "del_no":"删除失败",
            "get_no":"不存在",
            "off_error":"状态错误",
            "name_kong":"名字不能为空",
            "cerir_ok":"清理成功",
            "id_renshenhei":"人工审核未开启",
            "id_yesok":"已经处理",
            "tong_off":"通道关闭",
            "no_login":"没有登陆",
            "shouhoufang":"请稍后访问",
            "find_error":"查询错误",
            "type_error":"支付方式错误",
            "close_off":"充值关闭",
            "mchid_error":"mchid 错误",
            "orderid_error":"orderid 错误 < 12",
            "moeny_error":"moeny 错误",
            "time_error":"time 错误",
            "sign_error":"sign 错误",
            "productid_error":"productid 错误",
            "productid_off":"productid 关闭",
            "notify_no":"notify  不能为空",
            "code_error":"二维码地址错误",
            "code_no":"没有二维码",
            "fenpei_error":"分配错误",
            "orderid_no":"订单不存在",
            "orderid_off":"订单状态错误",
            "jine_cha":"金额相差较大",
            "code_yong":"已匹配无法删除",
            "pl_make":"批量生成",
            "jine_ge":"金额码数格式[1-100]",

            "jine_fanerror":"金额范围错误",
            "jine_jiange":"金额间隔小于1",
            "make_duo":"生成规则过多"
        },
    };
    if(YUYAN[lang]){
        return YUYAN[lang];
    }
    return YUYAN["cn"];
}

]