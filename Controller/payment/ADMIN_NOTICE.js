[//
    async function( kuo,Context,context){
        let LANG = this.LANG();
        let $DATA = Object.assign({},this.$DATA);
        let $hash = 'safetoken/'+kuo.adminid;
        $DATA.token = Mode("Tools").Uuid();
        var $THIS = this;
        await  Mem.Set($hash,$DATA.token);
        let  $kongzhi = kuo.Path.length > 0?kuo.Path[kuo.Path.length-1]:"get";
        let  $db = await db('payment_notice');
        let  $_GET = kuo.get;
        let  $_POST = kuo.post;
        let  $chushi = true; 
        let  $features = await Kuoplus(kuo.class);
        if($kongzhi == 'get'){
    
            let $page = Mode("Tools").Int($_GET['page']?$_GET['page']:1);
            let $limitx  =  Mode("Tools").Int($_GET['limit']?$_GET['limit']:10);
            if($limitx < 10 ){
                $limitx = 10;
            }else if($limitx > 100 ){
                $limitx = 100;
            }
            let $where ={};
            if(isset($_GET['type']) && $_GET['type'] != ""){
                $chushi = false; 
                $where['type'] = $_GET['type'];
            }
            
            if(isset($_GET['orderid']) && $_GET['orderid'] != ""){
                $chushi = false; 
                $where['orderid'] = $_GET['orderid'];
            }
        
            if(isset($_GET['atimestart']) && $_GET['atimestart'] != ""){
                $chushi = false; 
                $where['atime >'] =strtotime($_GET['atimestart']);
            }
            if(isset($_GET['atimeend']) && $_GET['atimeend'] != ""){
                $chushi = false; 
                $where['atime <'] =strtotime($_GET['atimeend']);
            }
            
            let $data  = await $db.Where($where).Limit($limitx,$page).Order("id desc").Select();
            let $total = await $db.Where($where).Total();
            if(!$data){
                $data= [];
            }
            $DATA.code = 0;
            $DATA.count = $total;
            if($page == 1 && $chushi){
                $DATA['type'] = $features['configure']['支付方式']?$features['configure']['支付方式']:[];
            }
    
            $DATA.data = $data;
    
    
        }else if($kongzhi == 'put'){
            let $id = Mode("Tools").Int($_POST['id']?$_POST['id']:0);
            let $data = await $db.Where({id: $id}).Find();
            if(!$data){
                $DATA.code = -1;
                $DATA.msg = LANG.admin_id_no;
                Context.body = JSON.stringify($DATA);
                return ;
            }

            let $POST = {'orderid' : 0};
            $POST['adminid'] = kuo.adminid;
            if($data['orderid'] == '0' &&  $_POST['orderid'] > 0){

                let $DB =  await db("payment_order");
                let $order = $DB.Where({'id' : $_POST['orderid'] }).Find();
                if(!$order){
                    $DATA.code = -1; $DATA.msg = LANG.orderid_no; Context.body = JSON.stringify($DATA);
                    return ;
                    
                }
                if( $order['off'] < 1 || $order['off'] == 2){
                    $DATA.code = -1; $DATA.msg = LANG.orderid_off; Context.body = JSON.stringify($DATA);
                    return ;
                 
                }
                $xiangcha = $data['moeny'] - $order['moeny'];
                if($xiangcha < -1 || $xiangcha > 1){
                    $DATA.code = -1; $DATA.msg = LANG.jine_cha; Context.body = JSON.stringify($DATA);
                    return ;
                   
                }
                let $pan = await $DB.Where({'id' : $order['id']}).Update({'off':2,'ctime':Mode("Tools").Time(),'paymoeny':$data['moeny'],'noticeid':$data['id']});
                if(!$pan){
                    $DATA.code = -1; $DATA.msg = LANG.put_no; Context.body = JSON.stringify($DATA);
                    return ;
                }
                $POST['orderid'] =  $order['id'];
                $POST['remarks'] = $_POST['remarks'];
            }

            if($POST['orderid'] < 1){
                $DATA.code = -1; $DATA.msg = LANG.put_no; Context.body = JSON.stringify($DATA);
                return ;
            }
            
            
            let $fan = await $db.Where({id: $id}).Update($POST);
            if($fan){
                if($POST['orderid'] > 0){
                    await $THIS.Notice_Post($POST['orderid'],$features);
                   
                }
                await Kuolog('adminlog',kuo.adminid,3,{'yuan':$data,'data':$POST},kuo.func_,kuo.class,kuo.ip);
                $DATA.data = $POST;
                $DATA.code =1;
                $DATA.msg = LANG.put_ok;
            }else{
                $DATA.code =-1;
                $DATA.msg = LANG.put_no;
            }
    
            
        }else if($kongzhi == 'add'){
    
        
            let $fan =await $db.Insert($_POST);
            if($fan){
                $DATA.code = 1;
                $DATA.msg = LANG.add_ok;
                await Kuolog('adminlog',kuo.adminid,4, $_POST ,kuo.func_,kuo.class,kuo.ip);
            }else{
                $DATA.code = -1;
                $DATA.msg = LANG.add_no;
            }
    
    
        }else if($kongzhi == 'del'){
    
            let $id = Mode("Tools").Int($_POST['id']?$_POST['id']:0);
            let $data = await $db.Where({id: $id}).Find();
    
            if(!$data){
                $DATA.code = -1;
                $DATA.msg = LANG.admin_id_no;
                Context.body = JSON.stringify($DATA);
                return ;
            }
    
            let $fan = await $db.Where({id: $id}).Delete();
            if($fan){
               
                await  Kuolog('adminlog',kuo.adminid,5, $data ,kuo.func_,kuo.class,kuo.ip);
                $DATA.code = 1;
                $DATA.msg = LANG.del_ok;
            }else{
                $DATA.code = -1;
                $DATA.msg = LANG.del_no;
            }
    
    
        }
        
        Context.body = JSON.stringify($DATA);
    }
    
]