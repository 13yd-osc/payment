[//
    async function( kuo,Context,context){
        Context.headers["content-type"] = 'text/html; charset=UTF-8';
        let LANG = this.LANG();
        let $CANSHU = kuo.Path;
        let $THIS = this;
        let $features  = await Kuoplus(kuo.class);   
        let $MCHID = $features['configure']['订单录入']['0']?$features['configure']['订单录入']['0']:"";
        let $MCKEY = $features['configure']['订单录入']['1']?$features['configure']['订单录入']['1']:"";
        let $TYPE  = $features['configure']['支付方式']?$features['configure']['支付方式']:[];
        let $UUID = $CANSHU['0']? $CANSHU['0']:"";//验证UUID
        let $ORDERID = Mode("Tools").Int($CANSHU['1']);//订单id
        let $TOKEN = $CANSHU['2']?$CANSHU['2']:"";//随机token
        let $SIGN = $CANSHU['3']?$CANSHU['3']:"";//签名
        let $TIME = Mode("Tools").Int( $CANSHU['4']?$CANSHU['4']:0);//到期时间
        let $GTIME = Mode("Tools").Int($features['configure']['过期时间']['0']?$features['configure']['过期时间']['0']:300);
        let time__  = Mode("Tools").Time();
        if($ORDERID < 1){
            Context.body =  $THIS.HtmlOut("订单错误!");
            return ;
        }
        if($SIGN != Md5($TIME+$MCHID+$ORDERID+$TOKEN+$MCKEY)+Md5($TIME+$UUID+$MCKEY)){
            Context.body =  $THIS.HtmlOut("订单错误!!");
            return ;
        }
        let $db = await db("payment_order");
        let $DATA =await $db.Where({'id' :$ORDERID}).Find();
        if(!$DATA){
            Context.body =  $THIS.HtmlOut("订单错误!!");
            return ;
            
        }
        if($TIME < time__ && $DATA['off'] != 2){
            Context.body =  $THIS.HtmlOut("订单过期!!");
            return ;
            
        }
        if($DATA['off'] == 3){
            Context.body =  $THIS.HtmlOut("订单过期!!");
            return ;
          
        }
        let $code = null;
        if($DATA['off'] == 1){ 
            $db = await $db.SetTable("payment_code");
            $code = await $db.Where({'id':$DATA['codeid']}).Find();
            if(!$code){
                Context.body =  $THIS.HtmlOut("二维码错误!!");
                return ;
            }
        }

        if($DATA['off'] == 2 && $DATA['productid'] == 0){

            if($DATA['return']!= ""  && strpos($DATA['return'], '://') !== false ){
                let $TICAN = {
                    'orderid' :$DATA['orderid'],
                    'off':$DATA['off'],
                    'remarks':$DATA['remarks'],
                    'id':$DATA['id']
                };
                Context.statusCode = 302;
                Context.headers["Location"] = ($DATA['return']+'?&'+getarray($TICAN));
                Context.body = "";
                
                return ;
            }else{
                Context.body =  $THIS.HtmlOut("已支付!!");
                return ;
            
            }
        }
  
        let $SHUOMING = $features['configure'][$TYPE[$DATA['type']]] && $features['configure'][$TYPE[$DATA['type']]]['1']?$features['configure'][$TYPE[$DATA['type']]]['1']:"请扫码支付";
        let $GUOTIME = $TIME -time__;
        let $QMING = Md5($MCHID+$SIGN+$DATA['orderid']+$MCKEY);
      
        let $TITLE = $features['configure']['站点标题']['0']?$features['configure']['站点标题']['0']:"站点标题";
        let HTMLX = `<!doctype html>
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
            <meta name="renderer" content="webkit">
            <meta name="HandheldFriendly" content="True"/>
            <meta name="MobileOptimized" content="360"/>
            <meta name="format-detection" content="telephone=no"/>
            <meta name="apple-mobile-web-app-capable" content="yes" />
            <meta name="apple-mobile-web-app-status-bar-style" content="black" />
            <title>${$TITLE}</title>
            <link rel="stylesheet" href="${CDNHOST}Tpl/layui/css/layui.css">
            <style>
            body{ background: #fafbfc;font-size: 16px;color: #444; font-family: "Microsoft YaHei";}
            .bodyx { margin: 10px auto 0px auto; max-width: 330px;min-height:100px; text-align:center;padding:15px 0;}
            .bodyx h1{font-size:20px;margin-bottom:10px;}
            .zhongse{padding:10px 30px;background:#fff; border-radius: 10px;margin-top:20px;}
            .ZHIFUPAY{font-size:30px;font-weight:bold;}
            img.qrcode{    width: 248px;
            
            border-radius: 5px;
            overflow: hidden;
            border: 1px solid #f2f2f2;margin-bottom:20px;}
            .footer{margin-top:10px;}
            .footer a{color:red;font-size:18px;}
            </style>
        </head>
        <body>
            <div class="bodyx">
                <h1>${$DATA['subject']!=""?$DATA['subject']:$TITLE}</h1>
                <p style="color: #CD5555;" id="xiaoxitishi">请输入以下金额支付，否则会支付失败</p>
                <div class="zhongse">`;
                
                if($DATA['off'] == 1){ 
                    let $codeimg = $code['codeimg'] != '' ? $code['codeimg']: ($features['configure'][$TYPE[$DATA['type']]]['0']?$features['configure'][$TYPE[$DATA['type']]]['0']:"");
               
                    HTMLX += `<p class="ZHIFUPAY">￥${$code['paymoeny']}</p>
                        <p style="padding:20px 0;"> ${$code['remarks']!=''?$code['remarks']: $SHUOMING}</p>
                        <p><img class="qrcode" src="${pichttp($codeimg)}" alt="${$code['remarks']!=''?$code['remarks']: $SHUOMING}"></p>
                        <p>剩 <span  id="daojimiao">${$GUOTIME} 秒</span></p>`;
                    
                 }else if($DATA['off'] == 0){ 
                    HTMLX += `<p class="ZHIFUPAY">等待匹配请稍后</p>
                        <p style="padding:20px 0;">￥${$DATA['moeny']}</p>
                        <p><img class="qrcode" src="${CDNHOST}Tpl/payment/pipei.png" alt="等待匹配请稍后"></p>
                        <p> 剩 <span id="daojimiao">${$GUOTIME} 秒</span></p>`;
                  }else if($DATA['off'] == 2){ 
                    HTMLX += `<p class="ZHIFUPAY">已支付</p>
                        <p style="padding:10px 0;">￥${$DATA['paymoeny']}</p>
                        
                        <pre style="padding:30px 0px;" >${$DATA['body']}</pre><button type="button" class="layui-btn layui-btn-normal" onclick="copyneir();">复制内容</button>
                        <input type="text" style="width:1px;height:1px;border-color:#fff;color: #fff;border:none;" id="copyfuzhi" value="${$DATA['body']}">
                    `;
                }
                   
                HTMLX += `</div>
                <div class="footer">
                    <p> <a href="${$features['configure']['联系客服']['0']?$features['configure']['联系客服']['0']:""}" target="_blank">如需咨询请 点击这里</a> ！</p>
                </div>
            </div>
        
        </body>
        </html>
        <script src="${CDNHOST}Tpl/jquery.js" charset="utf-8"></script>
        <script>
        function copyneir(){
            const input = document.querySelector('#copyfuzhi');
            input.select();
            if (document.execCommand('copy')) {
                document.execCommand('copy');
                alert("复制成功!");
           
            }
  
        }
        var GUOTIME = ${$GUOTIME};
        var OFF = ${$DATA['off']};
        var JISHIIQ = null;
        var ZURL = '${KuoLink([ kuo.class,'find',$DATA['orderid'],$QMING,$SIGN])}';
        function miaojishi(){
            if(GUOTIME%3 == 0 || GUOTIME <= 1){
                $.getJSON(ZURL,function(data){
                    if(data.code == 1){
                        if(data.data.off >= 2){
                            
                            clearInterval(JISHIIQ); 
                            JISHIIQ = null;
                            setTimeout(() => {
                                window.location.href = window.location.href;
                            }, 2000);
                            return ;
                        }else if(data.data.off == "0" && data.data.tiaourl){
                            window.location.href = data.data.tiaourl;
                            clearInterval(JISHIIQ); 
                            JISHIIQ = null;
                        }
                    }
                });
            }
            GUOTIME--;
            if(GUOTIME < 1){
                clearInterval(JISHIIQ); 
                JISHIIQ = null;
                window.location.href=window.location.href;
                return ;
            }
            $("#daojimiao").html(GUOTIME +' 秒');
        }
        
        $(function(){
            if(OFF != 2){
                JISHIIQ = setInterval("miaojishi()", 1000);
            }
        });
        </script>`;
         Context.body =  HTMLX;
    }
    
]