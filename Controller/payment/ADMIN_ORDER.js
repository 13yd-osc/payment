[//
    async function( kuo,Context,context){
        let LANG = this.LANG();
        let $DATA = Object.assign({},this.$DATA);
        let $hash = 'safetoken/'+kuo.adminid;
        $DATA.token = Mode("Tools").Uuid();
        await  Mem.Set($hash,$DATA.token);
        let  $kongzhi = kuo.Path.length > 0?kuo.Path[kuo.Path.length-1]:"get";
        let  $db = await db('payment_order');
        let  $_GET = kuo.get;
        let  $_POST = kuo.post;
        let  $chushi = true; 
        let  $THIS = this;
        let  $features = await Kuoplus(kuo.class);
        if($kongzhi == 'get'){
    
            let $TJNUM = $features['configure']['统计天数']['0']?$features['configure']['统计天数']['0']:7;
            if($TJNUM < 1){
                $TJNUM = 2;
            }
            if($TJNUM > 31){
                $TJNUM = 31;
            }

            let $page = Mode("Tools").Int($_GET['page']?$_GET['page']:1);
            let $limitx  =  Mode("Tools").Int($_GET['limit']?$_GET['limit']:10);
            if($limitx < 10 ){
                $limitx = 10;
            }else if($limitx > 100 ){
                $limitx = 100;
            }
            let $where ={};
            if(isset($_GET['type']) && $_GET['type'] != ""){
                $chushi = false; 
                $where['type'] = $_GET['type'];
            }
            if(isset($_GET['id']) && $_GET['id'] != ""){
                $chushi = false; 
                $where['id'] = $_GET['id'];
            }
        
            if(isset($_GET['off']) && $_GET['off'] != ""){
                $chushi = false; 
                $where['off'] = $_GET['off'];
            }
        
            if(isset($_GET['orderid']) && $_GET['orderid'] != ""){
                $chushi = false; 
                $where['orderid'] = $_GET['orderid'];
            }
            if(isset($_GET['noticeid']) && $_GET['noticeid'] != ""){
                $chushi = false; 
                $where['noticeid'] = $_GET['noticeid'];
            }
            if(isset($_GET['productid']) && $_GET['productid'] != ""){
                $chushi = false; 
                $where['productid'] = $_GET['productid'];
            }
        
            if(isset($_GET['atimestart']) && $_GET['atimestart'] != ""){
                $chushi = false; 
                $where['atime >'] =strtotime($_GET['atimestart']);
            }
            if(isset($_GET['atimeend']) && $_GET['atimeend'] != ""){
                $chushi = false; 
                $where['atime <'] =strtotime($_GET['atimeend']);
            }
            let $data  = await $db.Where($where).Limit($limitx,$page).Order("id desc").Select();
            let $total = await $db.Where($where).Total();
            if(!$data){
                $data= [];
            }
            $DATA.code = 0;
            $DATA.count = $total;
            if($page == 1 && $chushi){
               
                $DATA['type'] = $features['configure']['支付方式']?$features['configure']['支付方式']:[];
                let $time = Mode("Tools").Time() - 3600*24*$TJNUM;
                await $db.Where({'off':3,'atime <':$time}).Delete();
            }
    
            $DATA.data = $data;
    
    
        }else if($kongzhi == 'put'){
            let $id = Mode("Tools").Int($_POST['id']?$_POST['id']:0);
            let $data = await $db.Where({id: $id}).Find();
            if(!$data){
                $DATA.code = -1;
                $DATA.msg = LANG.admin_id_no;
                Context.body = JSON.stringify($DATA);
                return ;
            }
            $DATA.code = 1;
            let $fan = await $THIS.Notice_Post($data['id'],$features);
            $DATA.msg  = $fan;
            
        }else if($kongzhi == 'add'){
    
        
    
    
        }else if($kongzhi == 'del'){
    
           
    
        }
        
        Context.body = JSON.stringify($DATA);
    }
    
]