[//
    async function($CANSHU,$features){
        let $ID = Mode("Tools").Int($CANSHU);
        
        let $MCHID = $features['configure']['订单录入']['0']?$features['configure']['订单录入']['0']:"";
        let $MCKEY = $features['configure']['订单录入']['1']?$features['configure']['订单录入']['1']:"";
        let $db = await db("payment_order");
        let $DATA_ = await $db.Where({'id':$ID}).Find();
        if(!$DATA_){
            return '订单数据错误';
        }
        let TIME = Mode("Tools").Time();
        let $fan = null;
        let $POST_ = {};
        if($DATA_['notify'] != "" &&  strpos($DATA_['notify'] , '//') !== false ){
            $POST_ = {
                'mchid':$MCHID,
                'orderid':$DATA_['orderid'],
                'id':$DATA_['id'],
                'moeny':$DATA_['moeny'],
                'paymoeny':$DATA_['paymoeny'],
                'remarks':$DATA_['remarks'],
                'off':$DATA_['off'],
                'uid':$DATA_['uid'],
                'productid': $DATA_['productid'],
                'time':TIME,
                'token':Md5(Mode("Tools").Uuid())
            };
            $POST_['sign'] = Md5($POST_['mchid']+$POST_['orderid']+$POST_['id']+$POST_['moeny']+$POST_['paymoeny']+$POST_['remarks']+$POST_['off']+$POST_['uid']+$POST_['productid']+$POST_['time']+$POST_['token']+$MCKEY );
            $fan = await POST( Trim($DATA_['notify']),$POST_);

        }

        if($DATA_['off'] == 2 && $DATA_['productid'] > 0 && $DATA_['body'] == ""){
            $db = await $db.SetTable('payment_product');
            let $product = await $db.Where({'id':$DATA_['productid']}).Find();
            if($product){
                if($product['off'] == 2){
                    if( $product['type'] == '0'){

                        if($product['body'] != ""){
                            $db = await $db.SetTable('payment_order');
                            await $db.Where({'id':$DATA_['id']}).Update({'body':$product['body']});
                        }

                    }else{

                        if($product['notify'] != "" &&  strpos($product['notify'] , '//') !== false ){
                            $POST_ = {
                                'id' : $DATA_['id'],
                                'moeny':$DATA_['moeny'],
                                'paymoeny':$DATA_['paymoeny'],
                                'subject':$DATA_['subject'],
                                'remarks':$DATA_['remarks'],
                                'skuid': $product['skuid'],
                                'time':TIME
                            };
                            $POST_['sign'] = Md5($POST_['subject']+$POST_['remarks']+$POST_['id']+$POST_['moeny']+ $POST_['paymoeny']+ $POST_['skuid']+$POST_['time']+($product['akey']!= "" ?$product['akey']:$MCKEY));
                            let $body = await POST( Trim($product['notify']),$POST_);
                            if($body){
                                $db = await $db.SetTable('payment_order');
                                await $db.Where({'id': $DATA_['id']}).Update({'body': Mode("Tools").Xss($body) });
                            }
                        }
                    }
                }
            }
        }


        if($fan){
            return Mode("Tools").Xss($fan);
        }else{
            return "ok";
        }
    }
]