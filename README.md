插件名称：个人免签系统(仅供参考学习测试)   
系统名称：阔老板  
请勿用于违反国家法律法规的业务!  
官方网址：[https://www.kuosoft.com](https://www.kuosoft.com)  
开源代码协议 Apache License 2.0 详见 http://www.apache.org/licenses/   
## 插件说明 

个人免签系统单机版    

## 安装 

对应放入文件   
执行 node build.js br  
登陆管理后台   
管理后台 -》 插件管理 -》  输入 payment 立即安装     
前台页面 /payment/ 
插件管理 进行参数配置  

## 插件变量和函数  
  
find 查询订单  
HtmlOut 输出网页提示  
Notice_Post 异步通知  
notice 和app通信  
pay 支付订单提交  
payfind 订单支付界面    
paytest 支付调试  
Run shell 脚本 提现处理  